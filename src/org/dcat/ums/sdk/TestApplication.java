package org.dcat.ums.sdk;

import org.dcat.ums.sdk.common.Constant;

import android.app.Application;

public class TestApplication extends Application {

	public DcatAgent agent;

	@Override
	public void onCreate() {
		agent = DcatAgent.getDcatAgent();
		agent.init(this, Constant.ANDROID);
		super.onCreate();
	}

}
