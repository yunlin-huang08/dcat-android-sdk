package org.dcat.ums.sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	DcatAgent agent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		agent = ((TestApplication) getApplication()).agent;
		agent.onCreate(this);

		Button button = (Button) findViewById(R.id.button);

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(MainActivity.this, SecondActivity.class));
			}
		});
	}

	@Override
	protected void onDestroy() {
		agent.onDestroy(this);
		super.onDestroy();
	}

}
