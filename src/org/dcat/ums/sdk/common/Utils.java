package org.dcat.ums.sdk.common;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.dcat.ums.sdk.BuildConfig;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Utils {

	/**
	 * get APPKEY
	 * 
	 * @param context
	 * @return appkey
	 */
	public static String getAppKey(Context paramContext) {
		String umsAppkey;
		try {
			PackageManager localPackageManager = paramContext.getPackageManager();
			ApplicationInfo localApplicationInfo = localPackageManager.getApplicationInfo(
					paramContext.getPackageName(), 128);
			if (localApplicationInfo != null) {
				String str = localApplicationInfo.metaData.getString("D_APPKEY");
				if (str != null) {
					umsAppkey = str;
					return umsAppkey.toString();
				}
				if (BuildConfig.DEBUG)
					Log.e("UmsAgent", "Could not read D_APPKEY meta-data from AndroidManifest.xml.");
			}
		} catch (Exception localException) {
			if (BuildConfig.DEBUG) {
				Log.e("UmsAgent", "Could not read D_APPKEY meta-data from AndroidManifest.xml.");
				localException.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * checkPermissions
	 * 
	 * @param context
	 * @param permission
	 * @return true or false
	 */
	public static boolean checkPermissions(Context context, String permission) {
		PackageManager localPackageManager = context.getPackageManager();
		return localPackageManager.checkPermission(permission, context.getPackageName()) == PackageManager.PERMISSION_GRANTED;
	}

	/**
	 * check phone _state is readied ;
	 * 
	 * @param context
	 * @return
	 */
	public static boolean checkPhoneState(Context context) {
		PackageManager packageManager = context.getPackageManager();
		if (packageManager.checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) != 0) {
			return false;
		}
		return true;
	}

	/**
	 * get PackageName
	 * 
	 * @param context
	 * @return
	 */
	public static String getPackageName(Context context) {
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

		if (checkPermissions(context, "android.permission.GET_TASKS")) {
			ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
			return cn.getPackageName();
		} else {
			if (BuildConfig.DEBUG) {
				Log.e("lost permission", "android.permission.GET_TASKS");
			}

			return null;
		}

	}

	@SuppressWarnings("deprecation")
	public static String getAndroidId(Context context) {
		String androidId = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
		if (androidId == null) {
			androidId = android.provider.Settings.System.getString(context.getContentResolver(),
					android.provider.Settings.System.ANDROID_ID);
		}
		return androidId;
	}

	public static boolean isWiFiActive(Context inContext) {
		if (checkPermissions(inContext, "android.permission.ACCESS_WIFI_STATE")) {
			Context context = inContext.getApplicationContext();
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) {
					for (int i = 0; i < info.length; i++) {
						if (info[i].getTypeName().equals("WIFI") && info[i].isConnected()) {
							return true;
						}
					}
				}
			}
			return false;
		} else {
			if (BuildConfig.DEBUG) {
				Log.e("lost permission", "lost--->android.permission.ACCESS_WIFI_STATE");
			}

			return false;
		}
	}

	/**
	 * Testing equipment networking and networking WIFI
	 * 
	 * @param context
	 * @return true or false
	 */
	public static boolean isNetworkAvailable(Context context) {
		if (checkPermissions(context, "android.permission.INTERNET")) {
			ConnectivityManager cManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = cManager.getActiveNetworkInfo();
			if (info != null && info.isAvailable()) {
				return true;
			} else {
				if (BuildConfig.DEBUG) {
					Log.e("error", "Network error");
				}

				return false;
			}

		} else {
			if (BuildConfig.DEBUG) {
				Log.e(" lost  permission", "lost----> android.permission.INTERNET");
			}

			return false;
		}

	}

	public static boolean isNetworkTypeWifi(Context context) {
		// TODO Auto-generated method stub

		if (checkPermissions(context, "android.permission.INTERNET")) {
			ConnectivityManager cManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = cManager.getActiveNetworkInfo();

			if (info != null && info.isAvailable() && info.getTypeName().equals("WIFI")) {
				return true;
			} else {
				if (BuildConfig.DEBUG) {
					Log.e("error", "Network not wifi");
				}
				return false;
			}
		} else {
			if (BuildConfig.DEBUG) {
				Log.e(" lost  permission", "lost----> android.permission.INTERNET");
			}
			return false;
		}
	}

	/**
	 * Get the current networking
	 * 
	 * @param context
	 * @return WIFI or MOBILE
	 */
	public static String getNetworkType(Context context) {
		TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		int type = manager.getNetworkType();
		String typeString = "UNKNOWN";
		if (type == TelephonyManager.NETWORK_TYPE_CDMA) {
			typeString = "CDMA";
		}
		if (type == TelephonyManager.NETWORK_TYPE_EDGE) {
			typeString = "EDGE";
		}
		if (type == TelephonyManager.NETWORK_TYPE_EVDO_0) {
			typeString = "EVDO_0";
		}
		if (type == TelephonyManager.NETWORK_TYPE_EVDO_A) {
			typeString = "EVDO_A";
		}
		if (type == TelephonyManager.NETWORK_TYPE_GPRS) {
			typeString = "GPRS";
		}
		if (type == TelephonyManager.NETWORK_TYPE_HSDPA) {
			typeString = "HSDPA";
		}
		if (type == TelephonyManager.NETWORK_TYPE_HSPA) {
			typeString = "HSPA";
		}
		if (type == TelephonyManager.NETWORK_TYPE_HSUPA) {
			typeString = "HSUPA";
		}
		if (type == TelephonyManager.NETWORK_TYPE_UMTS) {
			typeString = "UMTS";
		}
		if (type == TelephonyManager.NETWORK_TYPE_UNKNOWN) {
			typeString = "UNKNOWN";
		}

		return typeString;
	}

	public static String getTime() {
		Date date = new Date();
		SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return localSimpleDateFormat.format(date);
	}
}
