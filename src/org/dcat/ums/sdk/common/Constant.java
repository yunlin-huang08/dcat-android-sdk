package org.dcat.ums.sdk.common;

public class Constant {
	public final static String ANDROID = "android";
	public final static String IOS = "ios";
	public final static String WPHONE = "wphone";

	public final static String BASE_URL_STRING = "http://192.168.1.117:8080";

	public final static String APP_PAGE_STRING = BASE_URL_STRING + "/v1/app/page";
	public final static String DEVICE_STRING = BASE_URL_STRING + "/v1/device";
}
