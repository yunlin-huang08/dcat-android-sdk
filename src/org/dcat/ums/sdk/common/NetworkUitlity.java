package org.dcat.ums.sdk.common;

import java.net.URLDecoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class NetworkUitlity {
	public static long paramleng = 256L;
	public static String DEFAULT_CHARSET = " HTTP.UTF_8";

	public static void post(String url, String data) {
		Log.d("NetworkUitlity", url);
		String returnContent = "";

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		try {
			StringEntity se = new StringEntity("content=" + data, HTTP.UTF_8);
			Log.d("postdata", "content=" + data);
			se.setContentType("application/x-www-form-urlencoded");
			httppost.setEntity(se);
			HttpResponse response = httpclient.execute(httppost);
			int status = response.getStatusLine().getStatusCode();
			Log.d("ums", status + "");
			String returnXML = EntityUtils.toString(response.getEntity());
			returnContent = URLDecoder.decode(returnXML);
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
