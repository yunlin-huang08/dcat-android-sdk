package org.dcat.ums.sdk;

import android.app.Activity;
import android.os.Bundle;

public class SecondActivity extends Activity {

	DcatAgent agent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		agent = ((TestApplication) getApplication()).agent;
		agent.onCreate(this);
	}

	@Override
	protected void onDestroy() {
		agent.onDestroy(this);
		super.onDestroy();
	}

}
